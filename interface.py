from gi.repository import Gtk
import botoes
import gcodeModule
#=================interface=================================

builder = Gtk.Builder()
builder.add_from_file("INTERFACE-CNC.glade")
handlers = {

# declaração das funções dos botões

    "terminar": Gtk.main_quit,
    
# movimento dos eixos    
    "evento_move_X+": botoes.Botoes.evento_move_Xx,
    "evento_move_X-": botoes.Botoes.evento_move_xX,
    "evento_move_Y+": botoes.Botoes.evento_move_Yy,
    "evento_move_Y-": botoes.Botoes.evento_move_yY,
    "evento_move_Z+": botoes.Botoes.evento_move_Zz,
    "evento_move_Z-": botoes.Botoes.evento_move_zZ,
    "origem_teste": botoes.Botoes.origem_teste,

    #"medir_mesa": medir_mesa,
    
    "grid": botoes.Botoes.grid,
    
    "origem": botoes.Botoes.origem,

    #"planer": planer,
    #"square": square,
    #"circle": circle,
    #"line": line,
    #"hole": hole,
    
#ler arquivos
    "read_file":gcodeModule.Gcode.read_file,
    "get_preview_filename": gcodeModule.Gcode.get_preview_filename,
    
#print coordenadas em tempo real
    
    "tool":botoes.Botoes.tool,
  
    
    
}

builder.connect_signals(handlers)
window1 = builder.get_object("janela_principal")
window1.show_all()

Gtk.main()
