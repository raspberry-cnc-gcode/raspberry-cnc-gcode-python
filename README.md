# README English#

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

# This project aims to create a program in python to work in conjunction with raspberry by running a cnc machine through gcode files. 
# The project is open and will help students to have more access to this type of information.
# Open and non-profit program, only to disseminate knowledge.

* Version: 1.0.4.alpha


### How do I get set up? ###

I'm using a Raspberry Pi3 model B, connected by a GPIO pin interface on BC557B transistors to send signals to DM422C step motor controllers. 
Base receives the pulse of raspberry and is attached in pull-donw with 10Kohms. 
Collector connected in raspberry Vcc 5V with resistors of 1kohms. 
And the transmitter is connected to the controller's PULL and DIR ports.

Check the model of your raspberry if it is the same as I use, as it can cause damage to the card if you use a different version: [Raspberry Pi3 Model B]

Sistema Linux raspberrypi 4.4.50-v7+ #970 SMP Mon Feb 20 19:18:29 GMT 2017 armv71 GNU/Linux 

The program is working connected to a hardware control and interface. 
Controlling a CNC router machine. Some functions of the program may be faulty because the sensors have not been identified. 
How to measure table function!

Download INTERFACE-CNC.glade 34.1 KB	raspberry-cnc-gcode (interface)
Keep in the same folder as the .py program


### Contribution guidelines ###

-Create graphical interface.
-By entering files for reading.     ok
-Converser of PDF files, DXF, DWG, to G-code.
-Implement reading and decoding of the G-code in steps for the engine.
-Terminate the arc, ellipse, straight, point, hole functions.
-Canvas, on-screen tool position display and real-time drawing.
-Plot drawing on canvas.
-Improving the program.
-Writing tests
-Code review


### Contact ###

Thiago Alencar Ribas
thiagoalencarribas@gmail.com
Engineering Student - Control and Automation  
College - Sociesc de Curitiba



# README Português-BR #

Este informativo normalmente documentaria quaisquer etapas necessárias para que seu aplicativo seja instalado e em execução.

### Para que serve esse repositório? ###

# Este projeto visa criar um programa em python para trabalhar em conjunto com framboesa, executando uma máquina cnc através de arquivos gcode.
# O projeto está aberto e ajudará os alunos a ter mais acesso a esse tipo de informação.
# Programa aberto e sem fins lucrativos, apenas para disseminar o conhecimento.

* Versão: 1.0.4.alpha


### Como faço para configurar? ###

Eu estou usando um Raspberry Pi3 modelo B, conectado por uma interface de pino GPIO em transistores BC557B para enviar sinais para DM422C motor de passo controladores.
Base recebe o pulso de framboesa e é anexado em pull-donw com 10Kohms.
Coletor conectado em framboesa Vcc 5V com resistores de 1kohms.
E o transmissor está conectado às portas PULL e DIR do controlador.

[Raspberry Pi3 Modelo B] [Raspberry Pi3 Modelo B] [Raspberry Pi3 Modelo B] [Raspberry Pi3 Modelo B]

Sistema Linux raspberrypi 4.4.50-v7 + # 970 SMP Seg Feb 20 19:18:29 GMT 2017 armv71 GNU / Linux

O programa está funcionando conectado a um controle de hardware e interface.
Controle da máquina de roteamento CNC. Algumas funções do programa podem estar com defeito porque os sensores não foram identificados.
Como medir a função de tabela!

Download INTERFACE-CNC.glade 34.1 KB	raspberry-cnc-gcode (interface)
Mantenha na mesma pasta que o programa .py


### Diretrizes de contribuição ###

-Criar interface gráfica.
- Entrando em arquivos para leitura.  ok
-Conversor de arquivos PDF, DXF, DWG, para G-código.
-Implementar leitura e decodificação do G-código em etapas para o motor. 
-Terminar as funções de arco, elipse, reta, ponto, buraco.
-Canvas, exibição de posição da ferramenta na tela e desenho em tempo real.
- Desenho de tela em tela.
- Melhorando o programa.
- Exames de escrita
-Revisão de código


### Contato ###

Thiago Alencar Ribas
Thiagoalencarribas@gmail.com
Estudante de Engenharia - Controle e Automação
Faculdade - Sociesc de Curitiba