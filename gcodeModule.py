# Just a test for the upload section
# This file is intended to hold the section that will do the gcode reading and convertion.
import re
from steeper import Steeper
import botoes
import namespace as Vars
from threading import Thread
import RPi.GPIO as GPIO 
import time

speed=xX1=yY1=zZ1=0
#xclock = 16
#xdir = 12
#yclock = 23
#ydir = 18
#zclock = 25
#zdir = 24
#xendc1 = 13
#yendc1 = 6
#zendc1 = 5
#tool = 20
#pulso=0.2369
GPIO.setmode(GPIO.BCM)



print("This gcode module will rock!")

class Gcode():
    
    def __init__(self):
        self.read_file()
        self.get_preview_filename
        
    def get_preview_filename(self):
        global filename
        filename=self.get_preview_filename()
        print(filename)
        return        
        
    def read_file(button12):
        global  filename
        arq = open(filename,'r')
        texto = arq.readlines()
        
        Mx = Steeper(Vars.xdir,Vars.xclock)
        My = Steeper(Vars.ydir,Vars.yclock)
        Mz = Steeper(Vars.zdir,Vars.zclock)
        
        for linha in texto :
            
            
            gcode=linha.split( )
            gG = re.findall("[gG]([\d]+)\D", linha)
            xX = re.findall("[xX]([\d\.\-]+)\D",linha)
            yY = re.findall("[yY]([\d\.\-]+)\D",linha)
            zZ = re.findall("[zZ]([\d\.\-]+)\D",linha)
            fF = re.findall("[fF]([\d\.\-]+)\D",linha)
            sS = re.findall("[sS]([\d\.\-]+)\D",linha)
            mM = re.findall("[mM]([\d\.\-]+)\D",linha)
            parada = re.findall("([\%%\.\-]+)\D",linha)
            move = 0   #atualiza o status de entrada do motor

            #print(gG,"x",xX,"y",yY,"z",zZ,fF)
            
            #Converte um elemento da lista para o tipo float
            if xX :
                
                xX1 = float(xX[0])
                xX1 =int(int((int(xX1*8))/(Vars.pulso))/8)
                #print("testando resolução da xX",xX1)
                move = 1
                
                
            if yY :
                
                yY1 = float(yY[0])
                yY1 =int(int((int(-yY1*8))/(Vars.pulso))/8)
                #print("testando resolução da yY",yY1)
                move = 1
                
                
            if zZ :
                
                zZ1 = float(zZ[0])
                zZ1 =int(int((int(-zZ1*8))/(Vars.pulso))/8)
                #print("testando resolução da zZ",zZ1)
                move = 1
                
                
                
            if gG :
                
                gG1 = float(gG[0])
                
                
            if fF : #melhorar relação de velocidades. não esta otimizado
                
                fF1 = float(fF[0])
                speed = 6/fF1
                #print("Velocidade de avanço =",fF1)
                
                
                
                
            if sS :
                
                sS1 = int(sS[0])
                print("RPM",sS1)

            if mM :
                
                mM1 = int(mM[0])
                
                if (mM1 == 3):
                    GPIO.setup(Vars.tool, GPIO.OUT)
                    GPIO.output(Vars.tool, GPIO.HIGH)
                    print("ferramenta ligada",mM1)
                    time.sleep(0.1)

                if (mM1 == 5):
                    GPIO.setup(Vars.tool, GPIO.OUT)
                    GPIO.output(Vars.tool, GPIO.LOW)
                    print("ferramenta desligada",mM1)
                    time.sleep(0.1)
                            
                
            if parada :
                
                parada1 = str(parada[0])
                print("Desenho concluido")
                

            if (move == 1 or xX1!=0 or yY1!=0 or zZ1!=0):
                #move_motores()
                move=0
                # Before calling this function, we have to be sure that the motors are at the origin!
                Mx.speed = My.speed = Mz.speed = speed
                tx = Thread(target=Mx.move, args=[xX1,])
                tx.start()
                ty = Thread(target=My.move, args=[yY1,])
                ty.start()
                tz = Thread(target=Mz.move, args=[zZ1,])
                tz.start()
                
                if Mx.moving:
                    tx.join()
                    #print(Mx.moving,"X")
                
                if My.moving:
                    ty.join()
                    #print(My.moving,"Y")
                
                if Mz.moving:
                    tz.join()
                    #print(Mz.moving,"Z")
                

                
                
        arq.close()
