# This project aims to create a program in python to work in conjunction with raspberry by running a cnc machine through gcode files. 
# The project is open and will help students to have more access to this type of information.
# Open and non-profit program, only to disseminate knowledge.
#
# Esse projeto tem por objetivo, cirar um programa em python para funcionar em conjunto com o raspberry acionando uma máquina cnc por meio de arquivos em gcode. 
# O projeto é aberto e servirá para ajudar estudantes a terem mais acesso a esse tipo de informação.
# Programa aberto e sem fins lucrativos, apenas visanto difundir conhecimento.
#
# contact: thiagoalencarribas@gmail.com
# v1.0.6.a.py


#======================================BIBLIOTECAS======================

import RPi.GPIO as GPIO                                                #DECLARAÇÃO DA BIBLIOTECA PARA OS PINOS DO RASPBERRY
import time                                                            #Biblioteca tempo, usat time.sleep() com tempo em segundos 
import math                                                            #biblioteca matematica
from gi.repository import Gtk                                          #biblioteca interface (feito com programa Glade)
import re                                                              #biblioteca de operações de expressão regular (manipular arquivos em lista)
from steeper import Steeper
from threading import Thread


#======================================CONFIGURAR PINOS RASPBERRY=======
xclock = 16
xdir = 12
yclock = 23
ydir = 18
zclock = 25
zdir = 24
xendc1 = 13
yendc1 = 6
zendc1 = 5
tool = 20

#======================================SAÍDAS===========================

GPIO.setmode(GPIO.BCM)                                                  #Declaração da configuração dos pinos do raspberry. Cuidado! BCM pode ser diferente de BOARD. 
GPIO.setwarnings(False)
GPIO.setup(xclock, GPIO.OUT)                                                #motor 01   PULL    (Clock) 
GPIO.setup(xdir, GPIO.OUT)                                                #motor 01   DIR     (direção usando nivel logico)
GPIO.setup(yclock, GPIO.OUT)                                                 #motor 02   PUL     (Clock)
GPIO.setup(ydir, GPIO.OUT)                                                #motor 02  DIR 
GPIO.setup(zclock, GPIO.OUT)                                                #motor 03  PUL 
GPIO.setup(zdir, GPIO.OUT)                                                #motor 03  DIR
GPIO.setup(tool, GPIO.OUT)

#adicionar pino para ligar a ferramenta

#============================ENTRADAS SENSOR ORIGEM MÁQUINA=============

GPIO.setup(yendc1, GPIO.IN, pull_up_down=GPIO.PUD_UP)                       #sensor fim de curso Y1
GPIO.setup(xendc1, GPIO.IN, pull_up_down=GPIO.PUD_UP)                       #sensor fim de curso X1
GPIO.setup(zendc1, GPIO.IN, pull_up_down=GPIO.PUD_UP)                       #sensor fim de curso Z1

#============================ENTRADAS SENSOR ORIGEM MÁQUINA OPOSTA======

#sensor fim de curso Y2
#sensor fim de curso X2
#sensor fim de curso Z2

#===================================DECLARAÇÃO DE VARIÁVEIS=============

speed= 0.005                                                            # VELOCIDADE DO CLOCK EM SEGNDOS
x=speed                                                                 #VELOCIDADE minima EM X
y=speed                                                                 #VELOCIDADE minima EM Y
z=speed                                                                 #VELOCIDADE minima EM Z

xx=622                                                                 #tamanho da mesa em CLOCK X  (o valor 622 é o tamanho atual e para reduzir tempo de testes de verificação mantive o valor "congelado" )
yy=642                                                                 #tamanho da mesa em CLOCK Y  (o valor 642 é o tamanho atual e para reduzir tempo de testes de verificação mantive o valor "congelado" )
zz=0                                                                   #tamanho da mesa em CLOCK Z  (unico que não congelei o valor pos as peças mudam de altura e ele lê a altura da peça por aqui)

xxx=yyy=zzz=0#int(622/2)                                                            #tamanho da mesa em CLOCK X  colocando a origem no centro da mesa
#yyy=int(642/2)                                                            #tamanho da mesa em CLOCK Y  colocando a origem no centro da mesa
#zzz=int(0)                                                                  #tamanho da mesa em CLOCK Z 

pulso=0.2369

desenho=0                                                              # variavel de controle para os desenhos

#tamando dos lados do retangulo em centimetros
desx=0                                                                 #é utilizada como raio do circulo tambem
desy=0

mm=35                                                                  #tamanho do desenho: quadrado(lado) ou circunferencia(raio) em mm

mmf=3                                                                #diametro da ferramenta em mm
ferramenta=(mmf/(pulso))                                          #desconto da ferramenta da origem


ciclo=10                                                              #é utilizado para repetir o passo no desenho do quadrado abaixando a ferramenta 1 pulso a cada ciclo

                                                      #passo dos motores em cm (vou colocar fuso esferico para melhorar)

#==================Criar função para essas alterações, sera utilizado no Gcode==========
clockcm=int (mm/(pulso*2))                   #ferramenta centro do pixel do desenho
#clockcm=int (((mm/10)/0.0433884)-(ferramenta/2)) #ferramenta no interior do desenho "furo"
#clockcm=int (((mm/10)/0.0433884)+(ferramenta/2)) #desenho externo ferramenta no exterior do desenho "recorte"

#====================tamanho do desenho convertido em passos===========================
desx=clockcm #tamanho em x passos (lado paralelogramo ou raio da circunferencia)
desy=clockcm #tamanho em y passos

#==============variaveis de controle para circunferencia================================

# delta usado para circunferencias, utilizando a resolução de passos.
# Calculamos em passos o comprimento do arco = 2*pi*desx, dividimos por 4 para pegar por quadrante.
# Este é o comprimento em passos da circunferencia, agora dividimos os 90 graus por esses passos para saber quantos pontos temos por grau de angulo.
# Nosso delta é uma fração desse angulo por passo. Despoi utilizamos ele para fazer a circunferencia de qualquer raio.

pi=(math.pi)                                                            #não sabia ao certo como declarar o "pi" mas esta funcionando
angulo=90                                                               #variavel usada no circulo
Delta=(90/((2*pi*desx)/4))

cont=1                                                                  #contador da plaina para inverter o sentido
#cont2=0      sem uso
#angulo2=90   sem uso

xmov=90
ax=1
kx=1
kx2=kx

ymov=0
ay=0
ky=0
ky2=ky

a=1           #VARIÁVEL DE CONTROLE
b=25          #controle de velocidade de avanço do corte b*speed (speed=0.005)
lado=0
turn=1 


filename=()
gcode=[]
xX1=0
yY1=0
zZ1=0
gG1=0
fF1=3000
sS1=0
mM1=0
parada1=0

#===================================DECLARAÇÃO DOS MOTORES==============
Mx = Steeper(xdir,xclock)
My = Steeper(ydir,yclock)
Mz = Steeper(zdir,zclock)

#===================================AVISOS AO OPERADOR==================

print("Pressione CTRL+C para sair")
time.sleep(0.1)
print("Verificando posicionamento")
time.sleep(0.1)
print("Aguarde...")
time.sleep(0.1)
print("Selecione uma opção no Menu")


#=======================CORPO DE EXECUÇÃO===============================

#======================Criando Janela do programa=======================


#=================================botão move eixo X+ ===================

def evento_move_Xx(button0,):
    xX1=0
    xX1=xX1+1
    moveX1()
    
    print(xX1)
    
def evento_move_xX(button1):
    global xxx
    moveX0()
    xxx=xxx-1
    print(xxx)
    
def evento_move_yY(button4):
    global yyy
    moveY1()
    yyy=yyy-1
    print(yyy)
    
def evento_move_Yy(button3):
    global yyy
    moveY0()
    yyy=yyy+1
    print(yyy)
    
def evento_move_zZ(button7):
    global zzz
    moveZ1()
    zzz=zzz-1
    print(zzz)
    
def evento_move_Zz(button6):
    global zzz
    moveZ0()
    zzz=zzz+1
    print(zzz)
    
#======================movendo motores===============================
def moveX1():#(button0)#X+
    global speed
    try:
        time.sleep(speed)
        GPIO.output(xdir, GPIO.HIGH)                                 #LOW DIREÇÃO ORIGEM
        GPIO.output(xclock, GPIO.HIGH)                                #CLOCK
        time.sleep(speed)
        GPIO.output(xclock, GPIO.LOW)                                 #CLOCK
        time.sleep(speed)
        #print("motor on X+")
    except KeyboardInterrupt:
        pass
#=================================botão move eixo X- ===================    
def moveX0():#(button1)#X-
    global speed
    try:
        time.sleep(speed)
        GPIO.output(xdir, GPIO.LOW)                                 #LOW DIREÇÃO ORIGEM
        GPIO.output(xclock, GPIO.HIGH)                                #CLOCK
        time.sleep(speed)
        GPIO.output(xclock, GPIO.LOW)                                 #CLOCK
        time.sleep(speed)
        #print("motor on X-")
    except KeyboardInterrupt:
        pass  
#=================================botão move eixo Y+ ===================
def moveY1():#(button3):#Y-
    global speed
    try:
        time.sleep(speed)
        GPIO.output(ydir, GPIO.HIGH)
        GPIO.output(yclock, GPIO.HIGH)
        time.sleep(speed)
        GPIO.output(yclock, GPIO.LOW)
        time.sleep(speed)                
    except KeyboardInterrupt:
        pass
#=================================botão move eixo Y- ===================    
def moveY0():#(button4):#Y+
    global speed
    try:
        time.sleep(speed)
        GPIO.output(ydir, GPIO.LOW)
        GPIO.output(yclock, GPIO.HIGH)
        time.sleep(speed)
        GPIO.output(yclock, GPIO.LOW)
        time.sleep(speed)                
    except KeyboardInterrupt:
        pass 
#=================================botão move eixo Z+ ===================
def moveZ1():#(button6):#Z+
    global speed
    try:
        time.sleep(speed)
        GPIO.output(zdir, GPIO.HIGH)
        GPIO.output(zclock, GPIO.HIGH)
        time.sleep(speed)
        GPIO.output(zclock, GPIO.LOW)
        time.sleep(speed)                
    except KeyboardInterrupt:
        pass
#=================================botão move eixo Z- ===================
def moveZ0():#(button7):#Z-
    global speed
    try:
        time.sleep(speed)
        GPIO.output(zdir, GPIO.LOW)
        GPIO.output(zclock, GPIO.HIGH)
        time.sleep(speed)
        GPIO.output(zclock, GPIO.LOW)
        time.sleep(speed)                
    except KeyboardInterrupt:
        pass 

#================================otimizar codigo dos botões==============



    

#=================================faz medir a mesa=============================
#=============================cuidado com os sensores fim de curso=============

        
#===========================move até origem no centro da mesa===================================
def origem(button8):
                
    global a, xxx, yyy, desx, desy, ciclo, desenho, ferramenta, zzz, speed
    speed=0.005
    xxx=(622/2)
    yyy=(642/2)
    zzz=0
    xxx=int(xxx-ferramenta)
    yyy=int(yyy-ferramenta)
    a=1
    

    
    try:


        a=2

        while (a==2):

            if GPIO.input(zendc1,) == GPIO.HIGH:                         #INDENTIFICAÇÃO DO SENSOR Z
                time.sleep(speed)   
                GPIO.output(zdir, GPIO.LOW)                         #LOW DIREÇÃO ORIGEM
                GPIO.output(zclock, GPIO.HIGH)                          #CLOCK
                time.sleep(speed)
                GPIO.output(zclock, GPIO.LOW)                           #CLOCK
                time.sleep(speed)
                zzz=zzz+1
                     
            if GPIO.input(yendc1,) == GPIO.HIGH:                         #INDENTIFICAÇÃO DO SENSOR Y
                time.sleep(speed) 
                GPIO.output(ydir, GPIO.LOW)                         #LOW DIREÇÃO ORIGEM
                GPIO.output(yclock, GPIO.HIGH)                         #CLOCK
                time.sleep(speed)
                GPIO.output(yclock, GPIO.LOW)                          #CLOCK
                time.sleep(speed)
                #yy=yy+1

            if GPIO.input(xendc1,) == GPIO.HIGH:                             #INDENTIFICAÇÃO DO SENSOR X
                time.sleep(speed) 
                GPIO.output(xdir, GPIO.HIGH)                                 #LOW DIREÇÃO ORIGEM
                GPIO.output(xclock, GPIO.HIGH)                                #CLOCK
                time.sleep(speed)
                GPIO.output(xclock, GPIO.LOW)                                 #CLOCK
                time.sleep(speed)
                #xx=xx+1

            if (GPIO.input(xendc1,) == GPIO.LOW and GPIO.input(yendc1,) == GPIO.LOW and GPIO.input(zendc1,) == GPIO.LOW):
                print("Verificação concluida!")
                a=3
                time.sleep(0.2)
                    
        while a==3:                                                     # NOVA ORIGEM- CENTRO DA MESA
                    
            if (xxx>0):
                time.sleep(speed)
                GPIO.output(xdir, GPIO.LOW)                                #HIGH DIREÇÃO ORIGEM
                GPIO.output(xclock, GPIO.HIGH)                                #CLOCK
                time.sleep(speed)
                GPIO.output(xclock, GPIO.LOW)                                 #CLOCK
                time.sleep(speed)
                xxx=xxx-1                                                 #tamanho da mesa em CLOCK

            if (xxx<0):
                time.sleep(speed)
                GPIO.output(xdir, GPIO.HIGH)                                #HIGH DIREÇÃO ORIGEM
                GPIO.output(xclock, GPIO.HIGH)                                #CLOCK
                time.sleep(speed)
                GPIO.output(xclock, GPIO.LOW)                                 #CLOCK
                time.sleep(speed)
                xxx=xxx+1              

            if (yyy>0):
                time.sleep(speed)
                GPIO.output(ydir, GPIO.HIGH)                                 #LOW DIREÇÃO ORIGEM
                GPIO.output(yclock, GPIO.HIGH)                                #CLOCK
                time.sleep(speed)
                GPIO.output(yclock, GPIO.LOW)                                 #CLOCK
                time.sleep(speed)
                yyy=yyy-1                                                 #tamanho da mesa em CLOCK

            if (yyy<0):
                time.sleep(speed)
                GPIO.output(ydir, GPIO.LOW)                                 #LOW DIREÇÃO ORIGEM
                GPIO.output(yclock, GPIO.HIGH)                                #CLOCK
                time.sleep(speed)
                GPIO.output(yclock, GPIO.LOW)                                 #CLOCK
                time.sleep(speed)
                yyy=yyy+1                                                 #tamanho da mesa em CLOCK


                
            if (yyy==0 and xxx==0):
                
                if(zzz>20):
                    speed=0.01

                if (zzz>0):
                    time.sleep(speed)
                    GPIO.output(zdir, GPIO.HIGH)                                 #LOW DIREÇÃO ORIGEM
                    GPIO.output(zclock, GPIO.HIGH)                                #CLOCK
                    time.sleep(speed)
                    GPIO.output(zclock, GPIO.LOW)                                 #CLOCK
                    time.sleep(speed)
                    zzz=zzz-1
                    print (zzz)

                if (zzz<0):
                    time.sleep(speed)
                    GPIO.output(zdir, GPIO.LOW)                                 #LOW DIREÇÃO ORIGEM
                    GPIO.output(zclock, GPIO.HIGH)                                #CLOCK
                    time.sleep(speed)
                    GPIO.output(zclock, GPIO.LOW)                                 #CLOCK
                    time.sleep(speed)
                    zzz=zzz+1
                
                if(zzz<20):
                    speed=0.08
                    print("speed=",speed)
                    
                if(zzz==0):
                    desenho=1
                    time.sleep(0.2)
                    a=3.5
                  
            if a==3.5:
                a=4
                print("Ferramenta no centro da mesa")
                print("Verifique se a ferramenta escolhida possui", mmf, "milimetros")
                time.sleep(0.2)

    except KeyboardInterrupt: # se precionar CTRL+C, interrompe o programa
        print("fim")

#======================================plaina=============================================
def planer(button13):
        
    global a, x, y, z, desenho, xxx, yyy, zzz, zz, speed, desx, desy, lado, turn
    ciclo=1000
    cont=100
    b=5
        
    try:
        while a==4:
                
            if desenho==1:
                    
                if (xxx>-desx):
                    GPIO.output(23, GPIO.LOW)     #HIGH DIREÇÃO ORIGEM
                    GPIO.output(24, GPIO.HIGH)    #CLOCK
                    time.sleep(x)
                    GPIO.output(24, GPIO.LOW)     #CLOCK
                    time.sleep(x)
                    xxx=xxx-1
                         
                if(yyy>-desy):                                #tamanho da mesa em CLOCK
                    GPIO.output(13, GPIO.HIGH)     #LOW DIREÇÃO ORIGEM
                    GPIO.output(26, GPIO.HIGH)    #CLOCK
                    time.sleep(y)
                    GPIO.output(26, GPIO.LOW)     #CLOCK
                    time.sleep(y)
                    yyy=yyy-1                       #tamanho da mesa em CLOCK
                         
                else:
                    desenho=2
                    lado=0
                    print(desenho, xxx, yyy, lado)
                          
            if desenho==2:
                  
                turn=1
                    
                while(zzz<zz and lado==0):
                          
                    z=speed*5
                        
                    GPIO.output(12, GPIO.LOW)     #LOW DIREÇÃO ORIGEM
                    GPIO.output(6, GPIO.HIGH)     #CLOCK
                    time.sleep(z)
                    GPIO.output(6, GPIO.LOW)     #CLOCK
                    time.sleep(z)
                    zzz=zzz+1                      #tamanho da mesa em CLOCK                  
                  
                    if (xxx<=desx and zzz==zz and cont>0):
                      
                        GPIO.output(23, GPIO.HIGH)     #HIGH DIREÇÃO ORIGEM
                        GPIO.output(24, GPIO.HIGH)    #CLOCK
                        time.sleep(x*b)
                        GPIO.output(24, GPIO.LOW)     #CLOCK
                        time.sleep(x*b)
                        xxx=xxx+1                    #tamanho da mesa em CLOCK
                    
                        cont=cont+1
                        lado=1                    
                  
                    if (xxx==desx and zzz==zz and yyy<(desy*2)):
                      
                        GPIO.output(13, GPIO.LOW)     #LOW DIREÇÃO ORIGEM
                        GPIO.output(26, GPIO.HIGH)    #CLOCK
                        time.sleep(y*b)
                        GPIO.output(26, GPIO.LOW)     #CLOCK
                        time.sleep(y*b)
                        yyy=yyy+1
                     
                        cont=cont*-1
                        desx=desx*-1
                     
                        print("|deslocamentox|",desx, "|   X   ",xxx, "|   Y   |", yyy, "|   Z   |", zzz)
                    
                    if (xxx>=desx and cont<0 and zzz==zz):
                        GPIO.output(23, GPIO.LOW)     #HIGH DIREÇÃO ORIGEM
                        GPIO.output(24, GPIO.HIGH)    #CLOCK
                        time.sleep(x*b)
                        GPIO.output(24, GPIO.LOW)     #CLOCK
                        time.sleep(x*b)
                        xxx=xxx-1                    #tamanho da mesa em CLOCK
                    
                    if ( GPIO.input(20,) == GPIO.LOW and yyy==0 ): #INDENTIFICAÇÃO DO SENSOR Z
                       GPIO.output(12, GPIO.HIGH)     #LOW DIREÇÃO ORIGEM
                       GPIO.output(6, GPIO.HIGH)     #CLOCK
                       time.sleep(z)
                       GPIO.output(6, GPIO.LOW)      #CLOCK
                       time.sleep(z)
                       zzz=zzz-1                     #tamanho da mesa em CLOCK
                   
                       cont=0

                    if( GPIO.input(20,) == GPIO.HIGH and ciclo>0 and turn==1 and yyy==-xxx):
                        ciclo=ciclo-1
                        turn=2
                        zz=zz-1
                        desenho=1
                        time.sleep(5)
                    
                    if(desx<0):
                        desx=desx*-1
                        print("Ciclos restantes=",ciclo)
                        time.sleep(5)
                         
                    if(a==4 and ciclo==0):
                        a=5

    except KeyboardInterrupt:# se precionar CTRL+C, interrompe o programa
            print("fim")
            
#=====================================quadrado===================================================            
def square(button14):
        
    global a, x, y, z, desenho, xxx, yyy, zzz, zz, speed, desx, desy, cont, lado, turn, ciclo
    b=5

    try:
        while a==4:
            turn=1
                		              
            if desenho==1:
                if (xxx>-desx):
                    GPIO.output(23, GPIO.LOW)     #HIGH DIREÇÃO ORIGEM
                    GPIO.output(24, GPIO.HIGH)    #CLOCK
                    time.sleep(x)
                    GPIO.output(24, GPIO.LOW)     #CLOCK
                    time.sleep(x)
                    xxx=xxx-1
                         
                if(yyy>-desy):                                #tamanho da mesa em CLOCK
                    GPIO.output(13, GPIO.HIGH)     #LOW DIREÇÃO ORIGEM
                    GPIO.output(26, GPIO.HIGH)    #CLOCK
                    time.sleep(y)
                    GPIO.output(26, GPIO.LOW)     #CLOCK
                    time.sleep(y)
                    yyy=yyy-1                       #tamanho da mesa em CLOCK
                         
                else:
                    desenho=2
                    lado=0
                    print(desenho, xxx, yyy, lado)
                          
            if desenho==2:

                if(zzz<zz and lado==0):
                    z=speed*15
                                                
                    GPIO.output(12, GPIO.LOW)     #LOW DIREÇÃO ORIGEM
                    GPIO.output(6, GPIO.HIGH)     #CLOCK
                    time.sleep(z)
                    GPIO.output(6, GPIO.LOW)     #CLOCK
                    time.sleep(z)
                    zzz=zzz+1                      #tamanho da mesa em CLOCK
                        
                if(zzz==zz):
                    time.sleep(0.2)
                                    
                if (xxx<desx and yyy==-desy and zzz==zz):
                    GPIO.output(23, GPIO.HIGH)     #HIGH DIREÇÃO ORIGEM
                    GPIO.output(24, GPIO.HIGH)    #CLOCK
                    time.sleep(x*b)
                    GPIO.output(24, GPIO.LOW)     #CLOCK
                    time.sleep(x*b)
                    xxx=xxx+1                  #tamanho da mesa em CLOCK
                                      
                if (xxx==desx and yyy<desy):
                    GPIO.output(13, GPIO.LOW)     #LOW DIREÇÃO ORIGEM
                    GPIO.output(26, GPIO.HIGH)    #CLOCK
                    time.sleep(y*b)
                    GPIO.output(26, GPIO.LOW)     #CLOCK
                    time.sleep(y*b)
                    yyy=yyy+1                                       
                     
                if (xxx>-desx and yyy==desy):
                    GPIO.output(23, GPIO.LOW)     #HIGH DIREÇÃO ORIGEM
                    GPIO.output(24, GPIO.HIGH)    #CLOCK
                    time.sleep(x*b)
                    GPIO.output(24, GPIO.LOW)     #CLOCK
                    time.sleep(x*b)
                    xxx=xxx-1                    #tamanho da mesa em CLOCK
                    lado=3
                   
                if (xxx==-desx and yyy>-desy and lado==3):
                    GPIO.output(13, GPIO.HIGH)     #LOW DIREÇÃO ORIGEM
                    GPIO.output(26, GPIO.HIGH)    #CLOCK
                    time.sleep(y*b)
                    GPIO.output(26, GPIO.LOW)     #CLOCK
                    time.sleep(y*b)
                    yyy=yyy-1
                                                                                                  
                if ( GPIO.input(20,) == GPIO.LOW and yyy==-desx and xxx==-desy and lado==3): #INDENTIFICAÇÃO DO SENSOR Z
                    z=speed*5
                    GPIO.output(12, GPIO.HIGH)     #LOW DIREÇÃO ORIGEM
                    GPIO.output(6, GPIO.HIGH)     #CLOCK
                    time.sleep(z)
                    GPIO.output(6, GPIO.LOW)      #CLOCK
                    time.sleep(z)
                    zzz=zzz-1                     #tamanho da mesa em CLOCK
                   
                if(turn>0 and lado==3 and GPIO.input(20,) == GPIO.HIGH):
                    zz=zz+1
                    ciclo=ciclo-1
                    turn=turn-1
                    lado=0
                    print(lado, zz, zzz, xxx, yyy, desenho, turn)
                    print("Ciclos restantes =", ciclo)
                       
                if ciclo==0:
                    print("Desenho Concluido")
                    a=5            

    except KeyboardInterrupt:# se precionar CTRL+C, interrompe o programa
        print("fim")
                 
#==================================circulo================================================
def circle(button15):
        
    global a, x, y, z, desenho, xxx, yyy, zzz, zz, speed, desx, desy, cont, lado, turn, ciclo
    global kx, kx2, ky, ky2, angulo, clockcm
    b=35

    try:
                      
        while a==4:
		              
            if desenho==1:
                if (xxx>-desx): #desloca ferramenta para origem do desenho em -x e y=0 para criar arco
                    GPIO.output(23, GPIO.LOW)     #HIGH DIREÇÃO ORIGEM
                    GPIO.output(24, GPIO.HIGH)    #CLOCK
                    time.sleep(x)
                    GPIO.output(24, GPIO.LOW)     #CLOCK
                    time.sleep(x)
                    xxx=xxx-1
                                                                
                else:
                    desenho=2
                    lado=0
                    print(desenho, xxx, yyy, lado)
                          
            if desenho==2:

                if(zzz<zz and lado==0):
                    z=speed*20  # '*' aumenta tempo de clock, '/' diminue tempo de clock (cuidado no alterar z=velocidade de descida da ferramenta)
                                                
                    GPIO.output(12, GPIO.LOW)     #LOW DIREÇÃO ORIGEM
                    GPIO.output(6, GPIO.HIGH)     #CLOCK
                    time.sleep(z)
                    GPIO.output(6, GPIO.LOW)     #CLOCK
                    time.sleep(z)
                    zzz=zzz+1                      #tamanho da mesa em CLOCK

                while(angulo>0 and zzz==zz):
                 
                        if(kx<kx2 and zzz==zz):#controle de pulsos por eixo em função do angulo
                   
                            GPIO.output(23, GPIO.HIGH)     #HIGH DIREÇÃO ORIGEM
                            GPIO.output(24, GPIO.HIGH)    #CLOCK
                            time.sleep(x*b)
                            GPIO.output(24, GPIO.LOW)     #CLOCK
                            time.sleep(x*b)
                            xxx=xxx+1
                            kx2=kx
                            #print("movendo x")                            
                             
                        if(ky>ky2 and zzz==zz):#controle de pulsos por eixo em função do angulo
                    
                            GPIO.output(13, GPIO.LOW)     #LOW DIREÇÃO ORIGEM
                            GPIO.output(26, GPIO.HIGH)    #CLOCK
                            time.sleep(y*b)
                            GPIO.output(26, GPIO.LOW)     #CLOCK
                            time.sleep(y*b)
                            yyy=yyy-1
                            ky2=ky
                            #print("movendo y")
                        
                        if angulo>0:
                            #o angulo decresce em função do delta para ter os pontos em todos os angulos
                            #em função do comprimento do arco.
                            
                            #para ter uma certa precisão de aproximação dos pontos em função do tamanho da figura
                            #multiplicamos o angulo pelo comprimento da imagem, arrredontamos "round" e dividimos
                            #pelo comprimento da imagem, assim não alteramos significativamente o desenho
                            #e melhoramos a aproximação dos pontos nos angulos.

                            #essa aproximação trabalha em função da resolução dos motores
                            #que se deixasse real os angulos, ele faria uma reta com 45° entre os eixos y x
                            angulo=((round((angulo-Delta)*clockcm))/clockcm)
                            kx=(round((math.sin(math.radians(angulo)))*clockcm)/clockcm)
                            ky=(round((math.cos(math.radians(angulo)))*clockcm)/clockcm)
                            print(angulo, "     ",kx,"     ", ky)

                if(angulo<=0):
                    angulo=90
                    kx=kx2=0
                    ky=ky2=1
                    a=5
                    print("proximo quadrante 2")
                                               
                while(angulo>0 and zzz==zz):
                     
                        if(kx>kx2 and zzz==zz):
                   
                            GPIO.output(23, GPIO.HIGH)     #HIGH DIREÇÃO ORIGEM
                            GPIO.output(24, GPIO.HIGH)    #CLOCK
                            time.sleep(x*b)
                            GPIO.output(24, GPIO.LOW)     #CLOCK
                            time.sleep(x*b)
                            xxx=xxx+1
                            kx2=kx
                            #print("movendo x")
                                                          
                        if(ky<ky2 and zzz==zz):
                    
                            GPIO.output(13, GPIO.HIGH)     #LOW DIREÇÃO ORIGEM
                            GPIO.output(26, GPIO.HIGH)    #CLOCK
                            time.sleep(y*b)
                            GPIO.output(26, GPIO.LOW)     #CLOCK
                            time.sleep(y*b)
                            yyy=yyy+1
                            ky2=ky
                            #print("movendo y")

                        if angulo>0:     
                            angulo=((round((angulo-Delta)*clockcm))/clockcm)
                            kx=(round((math.cos(math.radians(angulo)))*clockcm)/clockcm)
                            ky=(round((math.sin(math.radians(angulo)))*clockcm)/clockcm)
                            #print(angulo, "     ",kx,"     ", ky)
                                      
                if(angulo<=0):
                    angulo=90
                    kx=kx2=1
                    ky=ky2=0
                    a=5
                    print("proximo quadrante 3")
                     
                while(angulo>0 and zzz==zz):
                     
                        if(kx<kx2 and zzz==zz):
                   
                            GPIO.output(23, GPIO.LOW)     #HIGH DIREÇÃO ORIGEM
                            GPIO.output(24, GPIO.HIGH)    #CLOCK
                            time.sleep(x*b)
                            GPIO.output(24, GPIO.LOW)     #CLOCK
                            time.sleep(x*b)
                            xxx=xxx-1
                            kx2=kx
                            #print("movendo x")
                                                          
                        if(ky>ky2 and zzz==zz):
                    
                            GPIO.output(13, GPIO.HIGH)     #LOW DIREÇÃO ORIGEM
                            GPIO.output(26, GPIO.HIGH)    #CLOCK
                            time.sleep(y*b)
                            GPIO.output(26, GPIO.LOW)     #CLOCK
                            time.sleep(y*b)
                            yyy=yyy+1
                            ky2=ky
                            #print("movendo y")

                        if angulo>0:     
                            angulo=((round((angulo-Delta)*clockcm))/clockcm)
                            kx=(round((math.sin(math.radians(angulo)))*clockcm)/clockcm)
                            ky=(round((math.cos(math.radians(angulo)))*clockcm)/clockcm)
                            #print(angulo, "     ",kx,"     ", ky)
                                                                                                                     
                if(angulo<=0):
                         angulo=90
                         kx=kx2=0
                         ky=ky2=1
                         a=5
                         print("proximo quadrante 4")
                     
        
                while(angulo>0 and zzz==zz):
                     
                        if(kx>kx2 and zzz==zz):
                   
                            GPIO.output(23, GPIO.LOW)     #HIGH DIREÇÃO ORIGEM
                            GPIO.output(24, GPIO.HIGH)    #CLOCK
                            time.sleep(x*b)
                            GPIO.output(24, GPIO.LOW)     #CLOCK
                            time.sleep(x*b)
                            xxx=xxx-1
                            kx2=kx
                            #print("movendo x")
                                                          
                        if(ky<ky2 and zzz==zz):
                    
                            GPIO.output(13, GPIO.LOW)     #LOW DIREÇÃO ORIGEM
                            GPIO.output(26, GPIO.HIGH)    #CLOCK
                            time.sleep(y*b)
                            GPIO.output(26, GPIO.LOW)     #CLOCK
                            time.sleep(y*b)
                            yyy=yyy-1
                            ky2=ky
                            #print("movendo y")


                        if angulo>0:     
                            angulo=((round((angulo-Delta)*clockcm))/clockcm)
                            kx=(round((math.cos(math.radians(angulo)))*clockcm)/clockcm)
                            ky=(round((math.sin(math.radians(angulo)))*clockcm)/clockcm)
                            #print(angulo, "     ",kx,"     ", ky)

                             
                if(angulo<=0):
                        angulo=90
                        kx=kx2=0
                        ky=ky2=1
                        a=5
                        lado=2
                        time.sleep(z)
                        print("fim do circulo")
                        print(yyy, -desx, xxx, lado, desenho)

                while ( GPIO.input(20,) == GPIO.LOW and lado==2): #INDENTIFICAÇÃO DO SENSOR Z
                    z=speed*2
                    GPIO.output(12, GPIO.HIGH)     #LOW DIREÇÃO ORIGEM
                    GPIO.output(6, GPIO.HIGH)     #CLOCK
                    time.sleep(z)
                    GPIO.output(6, GPIO.LOW)      #CLOCK
                    time.sleep(z)
                    zzz=zzz-1                     #tamanho da mesa em CLOCK
                   
                   
                   
                if(turn>0 and xxx==desx and yyy==0 and GPIO.input(20,) == GPIO.HIGH):
                    zz=zz+1
                    turn=turn-1
                    lado=0
                    a=4
                    desenho=2
                    print(lado, zz, zzz, xxx, yyy, desenho, turn)
            
    except KeyboardInterrupt:# se precionar CTRL+C, interrompe o programa
        print("fim")

#=========================em construção==========================================
def line(button18):
    try:
        if(1+1==3):
            print(1)


    except KeyboardInterrupt:
        print("fim")

def hole(button19):
    try:
            
        if(1+1==2):
            print(tamanho)


    except KeyboardInterrupt:
        print("fim")

def grid(button16):
    global C, i
    i=0
    
    try:
        while i<10:
                        
    
            for i in range(10):
                 drawingarea1.create_line(50 * i, 0, 50 * i, 400)
                 drawingarea1.create_line(0, 50 * i, 400, 50 * i)

             
            
    except KeyboardInterrupt:
        print("fim")
        
def move_motores():
    global xxx, yyy, zzz, xX1, yY1, zZ1
    #print("esta funcionando")
    #print(xxx,xX1,yyy,yY1,zzz,zZ1)
    try:
        while (xxx!=xX1 or yyy!=yY1 or zzz!=zZ1):
            #print("esta funcionando 2")
            if xxx<xX1:
                moveX1()
                xxx=xxx+1
                #print("movendo x+",xxx,xX1)
            if xxx>xX1:
                moveX0()
                xxx=xxx-1
                #print("movendo x-",xxx,xX1)
            if yyy<yY1:
                moveY1()
                yyy=yyy+1
                #print("movendo y+",yyy,yY1)
            if yyy>yY1:
                moveY0()
                yyy=yyy-1
                #print("movendo y-",yyy,yY1)
            if zzz<zZ1:
                moveZ1()
                zzz=zzz+1
                print("movendo z+","Z=",zzz,"posição=",zZ1)
            if zzz>zZ1:
                moveZ0()
                zzz=zzz-1
                print("movendo z-","Z=",zzz,"posição=",zZ1)

            
                
    except KeyboardInterrupt:
        print("fim")

#seleciona o arquivo pelo botão
def get_preview_filename(self):
    global filename
    filename=self.get_preview_filename()
    print(filename)
    return
    
#abre e lê o arquivo
#precisa atribuir os valores da lista para as variáveis
#depois criar um interpretador de variaveis

def read_file(button12,):
    global filename, speed,xX1,yY1,zZ1
    arq = open(filename,'r')
    texto = arq.readlines()
    
    Mx = Steeper(xdir,xclock)
    My = Steeper(ydir,yclock)
    Mz = Steeper(zdir,zclock)
    
    for linha in texto :
        
        
        gcode=linha.split( )
        gG = re.findall("[gG]([\d]+)\D", linha)
        xX = re.findall("[xX]([\d\.\-]+)\D",linha)
        yY = re.findall("[yY]([\d\.\-]+)\D",linha)
        zZ = re.findall("[zZ]([\d\.\-]+)\D",linha)
        fF = re.findall("[fF]([\d\.\-]+)\D",linha)
        sS = re.findall("[sS]([\d\.\-]+)\D",linha)
        mM = re.findall("[mM]([\d\.\-]+)\D",linha)
        parada = re.findall("([\%%\.\-]+)\D",linha)
        move = 0   #atualiza o status de entrada do motor

        print(gG,"x",xX,"y",yY,"z",zZ,fF)
        
        #Converte um elemento da lista para fara o tipo float
        if xX :
            
            xX1 = float(xX[0])
            xX1 =int((xX1/2)/(pulso))
            #print("testando resolução da xX",xX1)
            move = 1
            
            
        if yY :
            
            yY1 = float(yY[0])
            yY1 =int((yY1/2)/(pulso))
            #print("testando resolução da yY",yY1)
            move = 1
            
            
        if zZ :
            
            zZ1 = float(zZ[0])
            zZ1 =int((-zZ1)/(pulso))
            print("testando resolução da zZ",zZ1, "Z=", zzz)
            move = 1
            
            
            
        if gG :
            
            gG1 = float(gG[0])
            
            
        if fF : #melhorar relação de velocidades. não esta otimizado
            
            fF1 = float(fF[0])
            speed = 15/fF1
            
            
            
            
        if sS :
            
            sS1 = int(sS[0])
            print("RPM",sS1)

        if mM :
            
            mM1 = int(mM[0])
            if (mM1 == 3):
                GPIO.output(tool, GPIO.HIGH)
                print("ferramenta ligada")
                time.sleep(0.5)

            if (mM1 == 5):
                GPIO.output(tool, GPIO.LOW)
                print("ferramenta desligada")
                time.sleep(0.5)
                        
            
        if parada :
            
            parada1 = str(parada[0])
            print("Desenho concluido")
            

        if (move == 1 or xX1!=0 or yY1!=0 or zZ1!=0):
            #move_motores()
            move=0
            # Before calling this function, we have to be sure that the motors are at the origin!
            Mx.speed = My.speed = Mz.speed = speed
            tx = Thread(target=Mx.move, args=[xX1,])
            tx.start()
            ty = Thread(target=My.move, args=[yY1,])
            ty.start()
            tz = Thread(target=Mz.move, args=[zZ1,])
            tz.start()
            
            if Mx.moving:
              tx.join()
            if My.moving:
              ty.join()
            if Mz.moving:
              tz.join()

            
            
    arq.close()
    
def origem_teste(button20):
    global xxx,yyy,zzz
    xxx=0
    yyy=0
    zzz=0
    print("Origem","X=",xxx,"Y=",yyy,"Z=",zzz)
       
#=================interface=================================

builder = Gtk.Builder()
builder.add_from_file("INTERFACE-CNC.glade")
handlers = {

# declaração das funções dos botões

    "terminar": Gtk.main_quit,
    
# movimento dos eixos    
    "evento_move_X+": evento_move_Xx,
    "evento_move_X-": evento_move_xX,
    "evento_move_Y+": evento_move_Yy,
    "evento_move_Y-": evento_move_yY,
    "evento_move_Z+": evento_move_Zz,
    "evento_move_Z-": evento_move_zZ,
    "origem_teste":origem_teste,

    #"medir_mesa": medir_mesa,
    
    "grid": grid,
    
    "origem": origem,

    "planer": planer,
    "square": square,
    "circle": circle,
    "line": line,
    "hole": hole,
    
#ler arquivos
    "read_file":read_file,
    "get_preview_filename": get_preview_filename,
    
#print coordenadas em tempo real
    
    
  
    
    
}

builder.connect_signals(handlers)
window1 = builder.get_object("janela_principal")
window1.show_all()

Gtk.main()
