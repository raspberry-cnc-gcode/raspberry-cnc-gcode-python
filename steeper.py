# implements the class Steeper that will drive the steeper motors
# this module should be importad as:
# from steeper import Steeper.
import RPi.GPIO as GPIO
import time

class Steeper:
       
	
    def __init__(self, ppin_tris, ppin_pulse):
        self.speed=20
        self.tris = ppin_tris			# do NOT change this number.
        self.pulse = ppin_pulse		# this neither.	# speed in steps per second
        self.dt = 1 / self.speed 		# time interval between steps
        self.position = 0	# Actual position. I'm assuming that the steepers will be at the initial position at the time of instantiation.
        self.moving = False
        GPIO.setmode(GPIO.BCM)

        

    def move(self, ptarget):
        
        if not self.moving and (self.position != ptarget):
            self.dt = self.speed
            self.moving = True
            #print(self.position,ptarget)  
            while(self.position != ptarget):
                GPIO.setup(self.pulse, GPIO.OUT)
                GPIO.setup(self.tris, GPIO.OUT)

                if (self.position > ptarget):
                    GPIO.output(self.tris, GPIO.LOW)         #HIGH:  Towards origin
                    GPIO.output(self.pulse, GPIO.HIGH)    	 #Rising CLOCK
                    time.sleep(self.dt)
                    GPIO.output(self.pulse, GPIO.LOW)     	 #Falling CLOCK
                    time.sleep(self.dt)
                    self.position -= 1
                else:
                    GPIO.output(self.tris, GPIO.HIGH)
                    GPIO.output(self.pulse, GPIO.HIGH)
                    time.sleep(self.dt)
                    GPIO.output(self.pulse, GPIO.LOW)
                    time.sleep(self.dt)
                    self.position += 1
            GPIO.setup(self.pulse, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
            GPIO.setup(self.tris, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
        self.moving = False
