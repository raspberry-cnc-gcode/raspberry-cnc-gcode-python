from steeper import Steeper
from threading import Thread
import namespace as Vars
import RPi.GPIO as GPIO
import time
# Use este arquivo para implementar as ações dos botões.

#xclock = 16
#xdir = 12
#yclock = 23
#ydir = 18
#zclock = 25
#zdir = 24
#xendc1 = 13
#yendc1 = 6
#zendc1 = 5
#tool = 20

xX1=0
yY1=0
zZ1=0
    
Mx = Steeper(Vars.xdir,Vars.xclock)

My = Steeper(Vars.ydir,Vars.yclock)

Mz = Steeper(Vars.zdir,Vars.zclock)

GPIO.setmode(GPIO.BCM) 
GPIO.setup(Vars.yendc1, GPIO.IN, pull_up_down=GPIO.PUD_UP)                       #sensor fim de curso Y1
GPIO.setup(Vars.xendc1, GPIO.IN, pull_up_down=GPIO.PUD_UP)                       #sensor fim de curso X1
GPIO.setup(Vars.zendc1, GPIO.IN, pull_up_down=GPIO.PUD_UP)
        
Mx.speed = My.speed = Mz.speed = 0.0016
mM1=0


class Botoes():
    
    def __init__(self):
        self.origem()
        self.origem_teste()
        
    def grid(button16):

        print("fim")

    def tool(button5):
        global mM1

        if (mM1 == 3):
            GPIO.setup(Vars.tool, GPIO.OUT)
            GPIO.output(Vars.tool, GPIO.HIGH)
            print("ferramenta ligada")
            time.sleep(0.5)
            mM1=5

        else:
            GPIO.setup(Vars.tool, GPIO.OUT)
            GPIO.output(Vars.tool, GPIO.LOW)
            print("ferramenta desligada")
            time.sleep(0.5)
            mM1=3
        
    def origem_teste(button20):
        global xX1,yY1,zZ1
        xX1=0
        yY1=0
        zZ1=0
        print("Origem","X=",xX1,"Y=",yY1,"Z=",zZ1)
        
    def evento_move_Xx(button0):
        global xX1
        xX1= xX1+1
        tx = Thread(target=Mx.move, args=[xX1,])
        tx.start()
        if Mx.moving:
            tx.join()
            
        print(xX1)
        
    def evento_move_xX(button1):
        global xX1
        
        xX1=xX1-1
        tx = Thread(target=Mx.move, args=[xX1,])
        tx.start()
        if Mx.moving:
            tx.join()
            
        print(xX1)
        
    def evento_move_yY(button4):
        global yY1
        
        yY1 = yY1-1
        ty = Thread(target=My.move, args=[yY1,])
        ty.start()
        if My.moving:
            ty.join()
            
        print(yY1)
               
    def evento_move_Yy(button3):
        global yY1
        
        yY1 = yY1+1
        ty = Thread(target=My.move, args=[yY1,])
        ty.start()
        if My.moving:
            ty.join()
            
        print(yY1)
        
    def evento_move_zZ(button7):
        global zZ1
        
        zZ1 = zZ1+1
        tz = Thread(target=Mz.move, args=[zZ1,])
        tz.start()
        if Mz.moving:
            tz.join()
            
        print(zZ1)
        
    def evento_move_Zz(button6):
        global zZ1
        
        zZ1 = zZ1-1
        tz = Thread(target=Mz.move, args=[zZ1,])
        tz.start()
        if Mz.moving:
            tz.join()
            
        print(zZ1)

    def origem(button8):         

        GPIO.setup(Vars.yendc1, GPIO.IN, pull_up_down=GPIO.PUD_UP)                       #sensor fim de curso Y1
        GPIO.setup(Vars.xendc1, GPIO.IN, pull_up_down=GPIO.PUD_UP)                       #sensor fim de curso X1
        GPIO.setup(Vars.zendc1, GPIO.IN, pull_up_down=GPIO.PUD_UP) 
        speed=0.001 
        xxx=yyy=1
        zzz=0         

        a=1
        
        try:

            a=1
            

            while (a==1):  #direciona os eixos para os sensores fim de curso para verificação de posição
                
                if GPIO.input(Vars.zendc1,) == GPIO.HIGH:                         #INDENTIFICAÇÃO DO SENSOR Z
                    global zZ1
                    zZ1 = zZ1-1
                    tz = Thread(target=Mz.move, args=[zZ1,])
                    tz.start()
                    zzz=zzz+1
                    if Mz.moving:
                        tz.join()
                        
                if GPIO.input(Vars.yendc1,) == GPIO.HIGH:
                    global yY1
                    yY1 = yY1+1
                    ty = Thread(target=My.move, args=[yY1,])
                    ty.start()
                    if My.moving:
                        ty.join()
                        
                if GPIO.input(Vars.xendc1,) == GPIO.HIGH:
                    global xX1
                    xX1 = xX1+1
                    tx = Thread(target=Mx.move, args=[-xX1,])
                    tx.start()
                    if Mx.moving:
                        tx.join()


                if (GPIO.input(Vars.xendc1,) == GPIO.LOW and GPIO.input(Vars.yendc1,) == GPIO.LOW and GPIO.input(Vars.zendc1,) == GPIO.LOW):
                    print("Verificação concluida!")
                    a=2
                    print("Movendo ferramenta para origem!")
                        
            while a==2: #movimentar para o centro da mesa.
                
                Mx.position=0
                tx = Thread(target=Mx.move, args=[3011,])
                tx.start()
                My.position=0
                ty = Thread(target=My.move, args=[-3021,])
                ty.start()
                
                if Mx.moving:
                    tx.join()
                    xxx=0
                if My.moving:
                    ty.join()
                    yyy=0
                    
                if(xxx==0 and yyy==0):
                    zz=zzz-15
                    Mz.position=0
                    tz = Thread(target=Mz.move, args=[zz,])
                    tz.start()
                    
                if Mz.moving:
                    tz.join()
                    
                    
                    if(zz):
                        Mz.speed = 0.001
                        zz=zz+15
                        tz = Thread(target=Mz.move, args=[zz,])
                        tz.start()
                        if Mz.moving:
                            tz.join()
                            
                    a=3.5
                      
                if a==3.5:
                    a=4
                    print("Ferramenta no centro da mesa")
                    print("Verifique se a ferramenta está conectada a tomada")
                    

        except KeyboardInterrupt: # se precionar CTRL+C, interrompe o programa
            print("fim")        
